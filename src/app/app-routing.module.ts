import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EventComponent} from './event/event.component';
import {EventCreateComponent} from './event-create/event-create.component';
import {EventDetailComponent} from './event-detail/event-detail.component';
import {EventUpdateComponent} from './event-update/event-update.component';

const routes: Routes = [
  {path: 'events', component: EventComponent},
  {path: 'events/create', component: EventCreateComponent},
  {path: 'events/detail/:id', component: EventDetailComponent},
  {path: 'events/update/:id', component: EventUpdateComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
