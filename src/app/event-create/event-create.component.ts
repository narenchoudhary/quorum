import { Component, OnInit } from '@angular/core';

import {Event} from '../event/event';
import {EventService} from '../event.service';


@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.css']
})
export class EventCreateComponent implements OnInit {

  public event: Event = new Event();
  public eventFormSubmitted = false;

  constructor(private eventService: EventService) { }

  ngOnInit() { }

  public onFormSubmit(): void {
    console.log('Running EventCreateComponent.onFormSubmit()');
    console.log('Event : ' + JSON.stringify(this.event));
    this.event = this.eventService.create(this.event);
    this.eventFormSubmitted = true;
  }
}
