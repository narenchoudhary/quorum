import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {Event} from '../event/event';
import {EventService} from '../event.service';

@Component({
  selector: 'app-event-update',
  templateUrl: './event-update.component.html',
  styleUrls: ['./event-update.component.css']
})
export class EventUpdateComponent implements OnInit {

  public event: Event;
  public eventFormSubmitted = false;

  constructor(
    private route: ActivatedRoute,
    private eventService: EventService
  ) { }

  ngOnInit() {
    this.getEvent();
  }

  getEvent(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.event = this.eventService.findById(id);
    console.log('Event : ' + JSON.stringify(this.event));
  }

  public onFormSubmit(): void {
    console.log('Running EventUpdateComponent.onFormSubmit()');
    console.log('Event : ' + JSON.stringify(this.event));
    this.event = this.eventService.update(this.event);
    this.eventFormSubmitted = true;
  }
}
