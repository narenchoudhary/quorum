import { Injectable } from '@angular/core';
import {Event} from './event/event';

export const EVENTS: Event[] = [
  {
    id: '1',
    name: 'Event Name',
    description: 'Event Description',
    startTime: '',
    endTime: '',
    isEventVisible: false
  },
  {
    id: '2',
    name: 'Event Name2',
    description: 'Event Description2',
    startTime: '',
    endTime: '',
    isEventVisible: false
  },
  {
    id: '3',
    name: 'Event Name3',
    description: 'Event Description3',
    startTime: '',
    endTime: '',
    isEventVisible: false
  }
];

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor() {}

  public findOne(): Event {
    console.log('EventService.findOne() called');
    return EVENTS[0];
  }

  public findById(id: string): Event {
    console.log('EventService.findById() called');
    console.log('id : ' + id);
    return EVENTS.find(x => x.id === id);
  }

  public findAll(): Event[] {
    console.log('EventService.findAll() called');
    return EVENTS;
  }

  public create(event: Event): Event {
    console.log('EventService.create() called');
    const id = Math.floor(Math.random() * 1000) + 1;
    event.id = String(id);
    console.log('Event saved is: ' + JSON.stringify(event));
    EVENTS.push(event);
    return event;
  }

  public update(event: Event): Event {
    console.log('EventService.update() called');
    console.log('Event saved is: ' + JSON.stringify(event));
    EVENTS.push(event);
    return event;
  }
}
