export class Event {
  public id: string;
  public name: string;
  public description: string;
  public startTime: string;
  public endTime: string;
  public isEventVisible: false;
}
