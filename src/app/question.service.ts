import { Injectable } from '@angular/core';
import {Question} from './question/question';

const questions: Question[] = [
  {
    id: '1',
    eventId: '1',
    questioner: 'questioner',
    questionBody: 'What?',
    tags: [
      'tag1',
      'tag2'
    ],
    hiddenByOrganizer: false,
    organizer: '',
    organizerRemark: '',
    host: 'host',
    answerBody: ',',
    upVotes: 20
  }
];

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor() { }

  public findAll(): Question[] {
    return questions;
  }

  public findById(id: string) {
    console.log('Running QuestionService.findById()');
    return questions.find(x => x.id === id);
  }

  public findByEventId(eventId: string): Question[] {
    console.log('Running QuestionService.findByEventId()');
    console.log('eventId: ' + eventId);
    return questions.filter(x => x.eventId === eventId);
  }

  public findByEventIdAndQuestioner(eventId: string, questioner: string) {
    console.log('Running QuestionService.findByEventIdAndQuestioner()');
    console.log('eventId: ' + eventId);
    console.log('questioner: ' + questioner);
    return questions.filter(x => x.eventId === eventId
      && x.questioner === questioner);
  }

  public create(question: Question): Question {
    console.log('Running QuestionService.create()');
    const id = Math.floor(Math.random() * 1000) + 1;
    question.id = String(id);
    questions.push(question);
    console.log('Question created : ' + JSON.stringify(question));
    return question;
  }

  public update(question: Question): Question {
    console.log('Running QuestionService.create()');
    console.log('Question created : ' + JSON.stringify(question));
    questions.push(question);
    return question;
  }
}
