import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {Question} from './question';
import {QuestionService} from '../question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  public questions: Question[];

  constructor(
    private route: ActivatedRoute,
    private questionService: QuestionService
  ) { }

  ngOnInit() {
    this.getQuestionsByEventId();
  }

  public getQuestionsByEventId(): void {
    console.log('Running QuestionComponent.getQuestions()');
    const id = this.route.snapshot.paramMap.get('id');
    console.log('eventId in path' + id);
    this.questionService.findByEventId(id);
    console.log('this.questions: ' + JSON.stringify(this.questions));
  }

}
