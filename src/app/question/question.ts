export class Question {
  public id: string;
  public eventId: string;
  public questioner: string;
  public questionBody: string;
  public tags: string[];
  public hiddenByOrganizer: false;
  public organizer: string;
  public organizerRemark: string;
  public host: string;
  public answerBody: string;
  public upVotes: number;
}
